﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace StartMenu
{
    public class LabelTab: Label
    {        
        public bool Selected {set;get;}

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (Selected == true)
            {
                this.BackColor = Color.White;
                // Draw borders
                e.Graphics.DrawLine(Pens.Black, 1, 0, this.Width - 1, 0);
                e.Graphics.DrawLine(Pens.Black, 0, 1, 0, this.Height - 2);
                e.Graphics.DrawLine(Pens.Black, 1, this.Height - 1, this.Width - 1, this.Height - 1);                
            }
            else
            {
                this.BackColor = Color.LightGray;
                // Draw borders
                // --
                e.Graphics.DrawLine(Pens.Black, 1, 0, this.Width - 1, 0);
                // |
                e.Graphics.DrawLine(Pens.Black, 0, 1, 0, this.Height-1);
                // --
                e.Graphics.DrawLine(Pens.Black, 1, this.Height-1, this.Width - 1, this.Height-1);
                //   |
                e.Graphics.DrawLine(Pens.Black, this.Width - 1, 0, this.Width - 1, this.Height - 1);
            }

            e.Graphics.FillRectangle(SystemBrushes.Control, 0, 0, 1, 1);
            e.Graphics.FillRectangle(SystemBrushes.Control, 0, this.Height, 1, 1);
        }
    }
}
