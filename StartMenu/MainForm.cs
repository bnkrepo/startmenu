﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Shell32;

namespace StartMenu
{
    public partial class MainForm : Form
    {
        //[http://www.geektieguy.com/2007/11/19/how-to-parse-special-lnk-files-aka-msi-shortcuts-aka-windows-installer-advertised-shortcuts-using-c/ 
        /* UINT MsiGetShortcutTarget( LPCTSTR szShortcutTarget, LPTSTR szProductCode, LPTSTR szFeatureId, LPTSTR szComponentCode ); */
        [DllImport("msi.dll", CharSet = CharSet.Auto)]
        static extern UInt32 MsiGetShortcutTarget(string targetFile, [Out] StringBuilder productCode, [Out] StringBuilder featureID, [Out] StringBuilder componentCode);

        public enum InstallState
        {
            NotUsed = -7,
            BadConfig = -6,
            Incomplete = -5,
            SourceAbsent = -4,
            MoreData = -3,
            InvalidArg = -2,
            Unknown = -1,
            Broken = 0,
            Advertised = 1,
            Removed = 1,
            Absent = 2,
            Local = 3,
            Source = 4,
            Default = 5
        }

        public const int MaxFeatureLength = 38;
        public const int MaxGuidLength = 38;
        public const int MaxPathLength = 1024;

        /* INSTALLSTATE MsiGetComponentPath( LPCTSTR szProduct, LPCTSTR szComponent, LPTSTR lpPathBuf, DWORD* pcchBuf); */
        [DllImport("msi.dll", CharSet = CharSet.Auto)]
        static extern InstallState MsiGetComponentPath( string productCode, string componentCode, [Out] StringBuilder componentPath, ref int componentPathBufferSize);

        //]
        // 
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindow(
        [MarshalAs(UnmanagedType.LPTStr)] string lpClassName,
        [MarshalAs(UnmanagedType.LPTStr)] string lpWindowName);

        [DllImport("user32.dll")]
        public static extern IntPtr SetParent(
         IntPtr hWndChild,      // handle to window
         IntPtr hWndNewParent   // new parent window
         );

        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        static extern bool ShellExecuteEx(ref SHELLEXECUTEINFO lpExecInfo);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct SHELLEXECUTEINFO
        {
            public int cbSize;
            public uint fMask;
            public IntPtr hwnd;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpVerb;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpFile;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpParameters;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpDirectory;
            public int nShow;
            public IntPtr hInstApp;
            public IntPtr lpIDList;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpClass;
            public IntPtr hkeyClass;
            public uint dwHotKey;
            public IntPtr hIcon;
            public IntPtr hProcess;
        }

        private const int SW_SHOW = 5;
        private const uint SEE_MASK_INVOKEIDLIST = 12;
        List<ProgramFileData> _speedLaunchList = new List<ProgramFileData>();
        List<ProgramFileData> _programFiles = new List<ProgramFileData>();
        // for getting file icons
        IntPtr hImgLarge;
        SHFILEINFO shinfo = new SHFILEINFO();
        Shell32.Shell _shell = new Shell32.Shell();

        delegate void EnableControlsDelegate(bool bEnable);
        delegate void AddToListDelegate(ListViewItem item);
        delegate void SetStatusDelegate(string strStatus);
        delegate ListView GetProgramListDelegate();
        delegate void AddGroupToListDelegate(ListViewGroup group);
        delegate void StartListUpdateDelegate(bool bUpdateStarted);
        
        bool _readingPrograms = false;
        bool _doingRefresh = false;
        bool _showProgramsOnly = false;
        bool _showGroups = false;
        Keypad _keypad = new Keypad();
        Settings _settings = new Settings();

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            EnableControls(false);
            _readingPrograms = true;
            backgroundWorker.RunWorkerAsync();
        }

        private void EnableControls(bool bEnable)
        {
            try
            {
                if (this.InvokeRequired)
                    this.Invoke(new EnableControlsDelegate(this.EnableControls), new object[] { bEnable });
                else
                {
                    txtSearch.Enabled = bEnable;
                    btnRefresh.Enabled = bEnable;
                    chkGroups.Enabled = bEnable;
                    chkPrograms.Enabled = bEnable;
                    btnSettings.Enabled = bEnable;
                }
            }
            catch (Exception)
            {
            }
        }

        private void LoadStartMenu()
        {
            try
            {
                //notifyIcon.ShowBalloonTip(2000, "Super Start Menu", "Reading program menu", ToolTipIcon.None);
                GetAllFiles(Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu));
                GetAllFiles(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu));
                GetAllFiles(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
                GetAllFiles(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Internet Explorer\\Quick Launch");
                GetAllFiles(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\Microsoft\\Internet Explorer\\Quick Launch");

                StartListUpdate(true);

                foreach (ProgramFileData file in _programFiles)
                {
                    AddToList(file);
                }

                StartListUpdate(false);

                SetStatus("Total Items Found: " + GetProgramList().Items.Count.ToString());
                //notifyIcon.ShowBalloonTip(2000, "Super Start Menu", "Indexing completed.", ToolTipIcon.Info);
            }
            catch (Exception)
            {

            }
        }

        private void StartListUpdate(bool bUpdateStarted)
        {
            try
            {
                if (this.InvokeRequired)
                    this.Invoke(new StartListUpdateDelegate(this.StartListUpdate), new object[] { bUpdateStarted });
                else
                {
                    if(bUpdateStarted == true)
                        listViewPrograms.BeginUpdate();
                    else
                        listViewPrograms.EndUpdate();
                }
            }
            catch (Exception)
            {
            }
        }

        private ListView GetProgramList()
        {
            try
            {
                if (this.InvokeRequired)
                    return (ListView)(this.Invoke(new GetProgramListDelegate(this.GetProgramList), new object[] { }));
                else
                {
                    return listViewPrograms;
                }
            }
            catch (Exception)
            {
            }

            return null;
        }

        private void AddToList(ProgramFileData file)
        {
            if (_showProgramsOnly == true)
            {
                string ext = Path.GetExtension(file.TargetExe).ToLower();
                string fileName = Path.GetFileName(file.Path).ToLower();

                if(ext == ".txt" == true || 
                    ext == ".rtf" == true || 
                    ext == ".doc" == true ||
                    ext == ".docx" == true ||
                    ext == ".pdf" == true ||
                    ext == ".html" == true ||
                    ext == ".htm" == true ||
                    ext == ".chm" == true ||
                    ext == ".hlp" == true ||
                    ext == ".url" == true ||
//                    ext == ".ico" == true || 
                    fileName.StartsWith("userguide") == true ||
                    fileName.StartsWith("user guide") == true ||
                    fileName.StartsWith("quickstart") == true ||
                    fileName.StartsWith("quick start") == true ||
                    //fileName.StartsWith("help") == true ||
                    fileName.StartsWith("userhelp") == true ||
                    fileName.StartsWith("user help") == true ||
                    fileName.StartsWith("gettingstarted") == true ||
                    fileName.StartsWith("getting started") == true || 
                    fileName.StartsWith("releasenote") == true ||
                    fileName.StartsWith("release note") == true ||
                    //fileName.StartsWith("uninstall") == true || 
                    fileName.StartsWith("uninst") == true ||
                    fileName.StartsWith("help") == true)
                    return;
            }

            ListViewItem item = null;

            if (file.LargeIconIndex != -1)
                item = new ListViewItem(Path.GetFileNameWithoutExtension(file.Path), file.LargeIconIndex);
            else
                item = new ListViewItem(Path.GetFileNameWithoutExtension(file.Path));

            if (_showGroups == true)
            {
                item.Group = GetProgramGroup(file.Group);

                /*
                int index = item.Group.Header.IndexOf('(');

                if (index != -1)
                {
                    item.Group.Header = item.Group.Header.Remove(index);
                }

                item.Group.Header += " (" + item.Group.Items.Count.ToString() + ")";
                */
            }

            item.Tag = file;
            AddToMainList(item);
        }

        private ListViewGroup GetProgramGroup(string group)
        {
            ListView listView = GetProgramList();
            int index;

            for (int i = 0; i < listView.Groups.Count; i++)
            {
                /*
                index = listView.Groups[i].Header.IndexOf('(');

                if (index != -1)
                {
                    listView.Groups[i].Header = listView.Groups[i].Header.Remove(index).TrimEnd();
                }
                */
                if (listView.Groups[i].Header == group)
                    return listView.Groups[i];
            }

            ListViewGroup grp = new ListViewGroup(group);
            AddGroupToList(grp);

            return grp;
        }

        private void AddGroupToList(ListViewGroup group)
        {
            try
            {
                if (this.InvokeRequired)
                    this.Invoke(new AddGroupToListDelegate(this.AddGroupToList), new object[] { group });
                else
                {
                    listViewPrograms.Groups.Add(group);
                }
            }
            catch (Exception)
            {
            }
        }

        private void AddToMainList(ListViewItem item)
        {
            try
            {
                if (this.InvokeRequired)
                    this.Invoke(new AddToListDelegate(this.AddToMainList), new object[] { item });
                else
                {
                    listViewPrograms.Items.Add(item);
                }
            }
            catch (Exception)
            {
            }
        }

        private void SetStatus(string strStatus)
        {
            try
            {
                if (this.InvokeRequired)
                    this.Invoke(new SetStatusDelegate(this.SetStatus), new object[] { strStatus });
                else
                {
                    lblTotal.Text = strStatus;
                }
            }
            catch (Exception)
            {
            }
        }

        private void GetAllFiles(string path)
        {
            if (Directory.Exists(path) == false)
                return;

            SetStatus("Processing " + path);
            //SetStatus("Processing...");

            foreach (string dir in Directory.GetDirectories(path))
            {                
                GetAllFiles(dir);
            }

            string targetFile = "", ext = "", group = "", tmp = "";

            foreach (string file in Directory.GetFiles(path))
            {
                ext = Path.GetExtension(file);

                if (ext != ".ini")
                {
                    if (file.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu)) == true)
                    {
                        tmp = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu);
                    }
                    else if (file.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu)) == true)
                    {
                        tmp = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu);
                    }
                    else if (file.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Internet Explorer\\Quick Launch") == true)
                    {
                        tmp = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Internet Explorer\\Quick Launch";
                    }
                    else if (file.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\Microsoft\\Internet Explorer\\Quick Launch") == true)
                    {
                        tmp = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\Microsoft\\Internet Explorer\\Quick Launch";
                    }
                    else if (file.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.Desktop)) == true)
                    {
                        tmp = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    }

                    if (tmp.Length > 0)
                    {

                        tmp = file.Remove(0, tmp.Length);
                        group = Path.GetDirectoryName(tmp).Trim();

                        if (group == "\\")
                            group = Path.GetFileNameWithoutExtension(Path.GetDirectoryName(file)); //"Quick Launch";
                        else if (group == "")
                            group = "Desktop";

                        if (group.StartsWith("\\") == true)
                            group = group.Remove(0, 1);

                    }

                    if (ext == ".lnk")
                    {
                        /*
                        if (file.Length > 0)
                            _programFiles.Add(new ProgramFileData(file, group, targetFile, GetLargeFileIcon(file)));
                        else
                        {
                            targetFile = GetLnkTarget(file);
                            _programFiles.Add(new ProgramFileData(file, group, targetFile, GetLargeFileIcon(targetFile)));
                        }
                        //*/
                        targetFile = GetLnkTarget(file);

                        if (targetFile.Length > 0)
                            _programFiles.Add(new ProgramFileData(file, group, targetFile, GetLargeFileIcon(targetFile)));
                        else
                            _programFiles.Add(new ProgramFileData(file, group, targetFile, GetLargeFileIcon(file)));
                    }
                    //else
                    //    _programFiles.Add(new ProgramFileData(file, group, "", GetLargeFileIcon(file)));
                }
            }
        }

        public string GetLnkTarget(string lnkPath)
        {
            try
            {
                /*
                string pathOnly = System.IO.Path.GetDirectoryName(lnkPath);
                string filenameOnly = System.IO.Path.GetFileName(lnkPath);

                Shell shell = new Shell();
                Folder folder = shell.NameSpace(pathOnly);
                FolderItem folderItem = folder.ParseName(filenameOnly);
                if (folderItem != null)
                {
                    Shell32.ShellLinkObject link = (Shell32.ShellLinkObject)folderItem.GetLink;
                    return link.Path;
                }

                return string.Empty;
                //*/
                StringBuilder product = new StringBuilder(MaxGuidLength + 1);
                StringBuilder feature = new StringBuilder(MaxFeatureLength + 1);
                StringBuilder component = new StringBuilder(MaxGuidLength + 1);
                uint ret = MsiGetShortcutTarget(lnkPath, product, feature, component);
                int pathLength = MaxPathLength;
                StringBuilder path = new StringBuilder(pathLength);
                InstallState installState = MsiGetComponentPath(product.ToString(), component.ToString(), path, ref pathLength);
                if (installState == InstallState.Local)
                {
                    return path.ToString();
                }
                else
                {
                    // try another way to get the data
                    //*
                    lnkPath = System.IO.Path.GetFullPath(lnkPath);
                    var dir = _shell.NameSpace(System.IO.Path.GetDirectoryName(lnkPath));
                    var itm = dir.Items().Item(System.IO.Path.GetFileName(lnkPath));
                    var lnk = (Shell32.ShellLinkObject)itm.GetLink;
                    if (lnk.Arguments.Length == 0)
                        return lnk.Path;
                    else
                        return lnkPath; // there are few arguments for this target. So we wont get the actual icon... hence this hack.
                }
                //*/
            }
            catch (Exception)
            {
                return "";
            }
        }

        private int GetLargeFileIcon(string strFilePath)
        {
            try
            {
                if (strFilePath == null || strFilePath.Length == 0)
                    return -1;

                //Use this to get the large Icon
                hImgLarge = Win32.SHGetFileInfo(strFilePath, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), Win32.SHGFI_ICON | Win32.SHGFI_LARGEICON);

                //The icon is returned in the hIcon member of the shinfo
                //struct
                Icon icon = (Icon)System.Drawing.Icon.FromHandle(shinfo.hIcon).Clone();
                Win32.DestroyIcon(shinfo.hIcon);

                imageListLarge.Images.Add(icon);
                return imageListLarge.Images.Count - 1;

            }
            catch (Exception e)
            {
                imageListLarge.Images.Add(imageListDefault.Images[0]);
                return imageListLarge.Images.Count - 1;
                //return -1;

            }

        }

        private void listViewPrograms_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewPrograms.SelectedItems.Count > 0 && listViewPrograms.SelectedItems[0] != null && listViewPrograms.SelectedItems[0].Tag != null)
            {
                lblPath.Visible = true;
                lblTarget.Visible = true;
                lblVersion.Visible = true;
                lblDesc.Visible = true;
                lblCompany.Visible = true;
                label1.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
                label5.Visible = true;
                label6.Visible = true;
                pbIcon32.Visible = true;
                btnProperty.Enabled = true;

                ProgramFileData data = ((ProgramFileData)listViewPrograms.SelectedItems[0].Tag);

                if (File.Exists(data.Path) == true)
                {
                    lblPath.ForeColor = Color.Black;

                    if (data.TargetExe != null && data.TargetExe.Length > 0)
                    {

                        lblTarget.Text = data.TargetExe;

                        try
                        {
                            FileVersionInfo info = FileVersionInfo.GetVersionInfo(data.TargetExe);
                            lblVersion.Text = info.FileVersion;
                            lblDesc.Text = info.FileDescription;
                            lblCompany.Text = info.CompanyName;
                        }
                        catch (Exception)
                        {
                        }
                    }
                    else
                    {
                        lblTarget.Text = "";
                        lblVersion.Text = "";
                        lblDesc.Text = "";
                        lblCompany.Text = "";
                    }
                    if (pbIcon32.Image != null)
                        pbIcon32.Image.Dispose();

                    try
                    {
                        pbIcon32.Image = imageListLarge.Images[data.LargeIconIndex];
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    lblPath.ForeColor = Color.Red;
                    lblTarget.Text = "";
                    lblVersion.Text = "";
                    lblDesc.Text = "";
                    lblCompany.Text = "";
                }

                lblPath.Text = data.Path;
            }
            else
            {
                lblPath.Visible = false;
                lblTarget.Visible = false;
                lblVersion.Visible = false;
                lblDesc.Visible = false;
                lblCompany.Visible = false;
                label1.Visible = false;
                label3.Visible = false;
                label4.Visible = false;
                label5.Visible = false;
                label6.Visible = false;
                pbIcon32.Visible = false;
                btnProperty.Enabled = false;
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            listViewPrograms.Items.Clear();
            lblTarget.Text = "";
            lblVersion.Text = "";
            lblDesc.Text = "";
            lblCompany.Text = "";
            lblPath.Visible = false;
            lblTarget.Visible = false;
            lblVersion.Visible = false;
            lblDesc.Visible = false;
            lblCompany.Visible = false;
            label1.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;

            pbIcon32.Visible = false;


            foreach (ProgramFileData file in _programFiles)
            {
                if (Path.GetFileNameWithoutExtension(file.Path).ToLower().Contains(txtSearch.Text.ToLower()) == true ||
                    file.Group.ToLower().Contains(txtSearch.Text.ToLower()) == true)
                    AddToList(file);
            }

            lblTotal.Text = "Total Items Found: " + listViewPrograms.Items.Count.ToString();
        }

        private void listViewPrograms_DoubleClick(object sender, EventArgs e)
        {
            if (listViewPrograms.SelectedItems.Count > 0 && listViewPrograms.SelectedItems[0] != null && listViewPrograms.SelectedItems[0].Tag != null)
            { 
                ProgramFileData data = ((ProgramFileData)listViewPrograms.SelectedItems[0].Tag);

                if (File.Exists(data.TargetExe) == true && data.TargetExe.EndsWith(".ico") == false)
                {
                    try
                    {
                        Process.Start(data.TargetExe);
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(exp.Message, "Exception Occured (TargetExe)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (File.Exists(data.Path) == true)
                {
                    try
                    {
                        Process.Start(data.Path);
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(exp.Message, "Exception Occured (Path)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void lblFilePath_Click(object sender, EventArgs e)
        {

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            _doingRefresh = true;
            listViewPrograms.Items.Clear();
            listViewPrograms.Groups.Clear();
            txtSearch.Clear();

            lblTarget.Text = "";
            lblVersion.Text = "";
            lblDesc.Text = "";
            lblCompany.Text = "";
            lblPath.Visible = false;
            lblTarget.Visible = false;
            lblVersion.Visible = false;
            lblDesc.Visible = false;
            lblCompany.Visible = false;
            label1.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            pbIcon32.Visible = false;


            foreach (ProgramFileData file in _programFiles)
            {
                AddToList(file);
            }

            lblTotal.Text = "Total Items Found: " + listViewPrograms.Items.Count.ToString();
            _doingRefresh = false;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            //btnFilter.Enabled = txtSearch.Text.Length > 0;

            if (txtSearch.Text.Length > 0)
                btnFilter_Click(null, null);
            else if(_doingRefresh == false)
                btnRefresh_Click(null, null);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //notifyIcon.Visible = false;
        }

        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            //if (WindowState == FormWindowState.Minimized)
            //    this.Hide();
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            this.Show();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            //if(Control.ModifierKeys == Keys.ControlKey && GetKeyState)
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            using (HelpForm help = new HelpForm())
            {
                help.ShowDialog();
            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadStartMenu();
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            EnableControls(true);
            _readingPrograms = false;
        }

        private void chkGroups_CheckedChanged(object sender, EventArgs e)
        {
            _showGroups = chkGroups.Checked;
            btnRefresh_Click(null, null);
        }

        private void chkDesktopOverlay_CheckedChanged(object sender, EventArgs e)
        {
            /*
            if (chkDesktopOverlay.Checked == true)
            {
                IntPtr hwndf = this.Handle;
                IntPtr hwndParent = FindWindow("ProgMan", null);
                SetParent(hwndf, hwndParent);
                //SetParent(hwndf, GetDesktopWindow());
                this.TopMost = false;

                Screen[] sc;
                sc = Screen.AllScreens;

                int nTaskBarHeight = Screen.PrimaryScreen.Bounds.Bottom - Screen.PrimaryScreen.WorkingArea.Bottom;

                Rectangle bounds = sc[0].Bounds;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.Location = new Point(bounds.X, bounds.Y);
                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height - nTaskBarHeight);
                this.StartPosition = FormStartPosition.Manual;
            }
            else
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                this.ClientSize = new System.Drawing.Size(892, 673);

            }
            */
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true && _readingPrograms == false)
            {
                _keypad.SetShortcuts(_speedLaunchList);
                _keypad.ShowDialog();
            }
        }

        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (_keypad.Visible == true && _readingPrograms == false)
                _keypad.Hide();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            //using (Settings settings = new Settings())
            {
                //settings.Shortcuts.Clear();
                _settings.KeypadList.Items.Clear();
                _settings.KeypadList.SmallImageList = imageListLarge;

                _settings.ProgramList.Items.Clear();
                _settings.ProgramList.SmallImageList = imageListLarge;
                

                foreach (ProgramFileData file in _programFiles)
                {
                    ListViewItem item = null;

                    if (file.LargeIconIndex != -1)
                        item = new ListViewItem(Path.GetFileNameWithoutExtension(file.Path), file.LargeIconIndex);
                    else
                        item = new ListViewItem(Path.GetFileNameWithoutExtension(file.Path));

                    item.Tag = file;

                    _settings.ProgramList.Items.Add(item);
                }

                _settings.ShowDialog();

                // after dialog is closed

                //_keypad.SetShortcuts(_settings.Shortcuts);
            }
        }

        private void chkPrograms_CheckedChanged(object sender, EventArgs e)
        {
            _showProgramsOnly = chkPrograms.Checked;
            btnRefresh_Click(null, null);
        }

        public static void ShowFileProperties(string Filename)
        {
            SHELLEXECUTEINFO info = new SHELLEXECUTEINFO();
            info.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(info);
            info.lpVerb = "properties";
            info.lpFile = Filename;
            info.nShow = SW_SHOW;
            info.fMask = SEE_MASK_INVOKEIDLIST;
            ShellExecuteEx(ref info);
        }

        private void btnProperty_Click(object sender, EventArgs e)
        {
            if (lblTarget.Text.Length > 0 && File.Exists(lblTarget.Text) == true)
            {
                ShowFileProperties(lblTarget.Text);
            }
            else if (lblPath.Text.Length > 0 && File.Exists(lblPath.Text) == true)
            {
                ShowFileProperties(lblPath.Text);
            }
        }

        private void addToSpeedDialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listViewPrograms.SelectedItems.Count != 0)
            {
                ProgramFileData data = ((ProgramFileData)listViewPrograms.SelectedItems[0].Tag);

                if (data != null)
                {
                    _speedLaunchList.Add(data);
                }
            }
        }

        private void addToSpeedDialToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            addToSpeedDialToolStripMenuItem.Enabled = _speedLaunchList.Count <= 9;// max 9 items.
        }
        
        private void contextMenuStripListView_Opening(object sender, CancelEventArgs e)
        {
            if (listViewPrograms.SelectedItems.Count == 0)
                e.Cancel = true;
        }
    }

    public class ProgramFileData
    {
        public string Path { set; get; }
        public string TargetExe { set; get; }
        //public int SmallIconIndex { set; get; }
        public int LargeIconIndex { set; get; }
        public string Group { set; get; }

        public ProgramFileData(string path, string group, string targetExe, int largeIconIndex)
        {
            Path = path;
            Group = group;
            TargetExe = targetExe;
            LargeIconIndex = largeIconIndex;
        }
    }
    // Code required to get icon associated with .url file
    [StructLayout(LayoutKind.Sequential)]
    public struct SHFILEINFO
    {
        public IntPtr hIcon;
        public IntPtr iIcon;
        public uint dwAttributes;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string szDisplayName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
        public string szTypeName;
    };

    public class Win32
    {
        public const uint SHGFI_DISPLAYNAME = 0x00000200;
        public const uint SHGFI_TYPENAME = 0x400;
        public const uint SHGFI_ICON = 0x100;
        public const uint SHGFI_LARGEICON = 0x0; // 'Large icon
        public const uint SHGFI_SMALLICON = 0x1; // 'Small icon

        [DllImport("shell32.dll")]
        public static extern IntPtr SHGetFileInfo(string pszPath, uint
        dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);

        [DllImport("User32.dll")]
        public static extern int DestroyIcon(IntPtr hIcon);

        [DllImport("mpr.dll")]
        public static extern int WNetGetUniversalName(string lpLocalPath,
                                                int dwInfoLevel,
                                                IntPtr lpBuffer,
                                                ref int lpBufferSize);

    }
}
