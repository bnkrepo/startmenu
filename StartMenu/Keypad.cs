﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;

namespace StartMenu
{
    public partial class Keypad : Form
    {
        // for getting file icons
        IntPtr hImgLarge;
        SHFILEINFO shinfo = new SHFILEINFO();

        public Keypad()
        {
            InitializeComponent();
        }

        private void Keypad_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyValue == 17) // CTRL key is up
                this.Close();
        }

        private void Keypad_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {

            }
        }

        private void StartProram(Button button)
        {
            if (button.Tag != null)
            {
                ProgramFileData data = ((ProgramFileData)button.Tag);

                if (File.Exists(data.Path) == true)
                {
                    Process.Start(data.Path);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StartProram(button1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StartProram(button2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            StartProram(button3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            StartProram(button4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            StartProram(button5);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            StartProram(button6);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            StartProram(button7);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            StartProram(button8);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            StartProram(button9);
        }

        internal void SetShortcuts(List<ProgramFileData> list)
        {
            if (list.Count > 0)
            {
                button1.Tag = list[0];

                if (button1.Image != null)
                {
                    button1.Image.Dispose();
                    button1.Image = null;
                }

                button1.Image = GetLargeFileIcon(((ProgramFileData)list[0]).TargetExe);
            }
            if (list.Count > 1)
            {
                button2.Tag = list[1];

                if (button2.Image != null)
                {
                    button2.Image.Dispose();
                    button2.Image = null;
                }

                button2.Image = GetLargeFileIcon(((ProgramFileData)list[1]).TargetExe);
            }
            if (list.Count > 2)
            {
                button3.Tag = list[2];

                if (button3.Image != null)
                {
                    button3.Image.Dispose();
                    button3.Image = null;
                }

                button3.Image = GetLargeFileIcon(((ProgramFileData)list[2]).TargetExe);
            }
            if (list.Count > 3)
            {
                button4.Tag = list[3];

                if (button4.Image != null)
                {
                    button4.Image.Dispose();
                    button4.Image = null;
                }

                button4.Image = GetLargeFileIcon(((ProgramFileData)list[3]).TargetExe);
            }
            if (list.Count > 4)
            {
                button5.Tag = list[4];

                if (button5.Image != null)
                {
                    button5.Image.Dispose();
                    button5.Image = null;
                }

                button5.Image = GetLargeFileIcon(((ProgramFileData)list[4]).TargetExe);
            }
            if (list.Count > 5)
            {
                button6.Tag = list[5];

                if (button6.Image != null)
                {
                    button6.Image.Dispose();
                    button6.Image = null;
                }

                button6.Image = GetLargeFileIcon(((ProgramFileData)list[5]).TargetExe);
            }
            if (list.Count > 6)
            {
                button7.Tag = list[6];

                if (button7.Image != null)
                {
                    button7.Image.Dispose();
                    button7.Image = null;
                }

                button7.Image = GetLargeFileIcon(((ProgramFileData)list[6]).TargetExe);
            }
            if (list.Count > 7)
            {
                button8.Tag = list[7];

                if (button8.Image != null)
                {
                    button8.Image.Dispose();
                    button8.Image = null;
                }

                button8.Image = GetLargeFileIcon(((ProgramFileData)list[7]).TargetExe);
            }
            if (list.Count > 8)
            {
                button9.Tag = list[8];

                if (button9.Image != null)
                {
                    button9.Image.Dispose();
                    button9.Image = null;
                }

                button9.Image = GetLargeFileIcon(((ProgramFileData)list[8]).TargetExe);
            }
        }

        private Image GetLargeFileIcon(string strFilePath)
        {
            try
            {
                if (strFilePath == null || strFilePath.Length == 0)
                    return null;

                //Use this to get the large Icon
                hImgLarge = Win32.SHGetFileInfo(strFilePath, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), Win32.SHGFI_ICON | Win32.SHGFI_LARGEICON);

                //The icon is returned in the hIcon member of the shinfo
                //struct
                Icon icon = (Icon)System.Drawing.Icon.FromHandle(shinfo.hIcon).Clone();
                Win32.DestroyIcon(shinfo.hIcon);

                //imageListLarge.Images.Add(icon);
                return icon.ToBitmap();

            }
            catch (Exception e)
            {
                return null;

            }

        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            ShowLabel(((Button)sender));
        }

        private void ShowLabel(Button button)
        {
            if (button.Tag != null)
            {
                ProgramFileData data = ((ProgramFileData)button.Tag);
                lblProgram.Text = Path.GetFileNameWithoutExtension(data.Path);
            }
        }

        private void Keypad_MouseMove(object sender, MouseEventArgs e)
        {
            lblProgram.Text = "";
        }

        private void removeFromSpeedLaunchToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
