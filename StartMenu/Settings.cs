﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace StartMenu
{
    public partial class Settings : Form
    {
        List<ProgramFileData> _programFiles = new List<ProgramFileData>();

        public List<ProgramFileData> Shortcuts
        {
            get { return _programFiles; }
            set { _programFiles = value; }
        }

        public ListView ProgramList
        {
            get { return listViewPrograms; }
            set { listViewPrograms = value; }
        }

        public ListView KeypadList
        {
            get { return listViewKeypad; }
            set { listViewKeypad = value; }
        }

        public Settings()
        {
            InitializeComponent();
        }

        private void listViewPrograms_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAdd.Enabled = listViewPrograms.SelectedIndices.Count > 0 && listViewPrograms.SelectedIndices[0] != -1;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listViewKeypad_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Enabled = listViewKeypad.SelectedIndices.Count > 0 && listViewKeypad.SelectedIndices[0] != -1;
        }

        private void listViewPrograms_DoubleClick(object sender, EventArgs e)
        {
            btnAdd.PerformClick();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (listViewKeypad.Items.Count >= 9)
            {
                MessageBox.Show("Maximum 9 entries can be added to Keypad.", "Maximum entries for Keypad", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (listViewPrograms.SelectedItems.Count > 0 && listViewPrograms.SelectedItems[0] != null && listViewPrograms.SelectedItems[0].Tag != null)
            {
                bool found = false;
                //check if this item is already included
                for (int i = 0; i < listViewKeypad.Items.Count; i++)
                {
                    if (listViewKeypad.Items[i].Text == listViewPrograms.SelectedItems[0].Text)
                    {
                        found = true;
                        break;
                    }
                }

                if (found == true)
                {
                    MessageBox.Show("Selected item already exists on Keypad.", "Duplicate entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    Shortcuts.Add((ProgramFileData)listViewPrograms.SelectedItems[0].Tag);
                    listViewKeypad.Items.Add((ListViewItem)listViewPrograms.SelectedItems[0].Clone());
                }
            }
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            listViewKeypad.Items.Clear();

            foreach (ProgramFileData file in Shortcuts)
            {
                ListViewItem item;

                if (file.LargeIconIndex != -1)
                    item = new ListViewItem(Path.GetFileNameWithoutExtension(file.Path), file.LargeIconIndex);
                else
                    item = new ListViewItem(Path.GetFileNameWithoutExtension(file.Path));
                item.Tag = file;
                listViewKeypad.Items.Add(item);
            }
        }
    }
}
